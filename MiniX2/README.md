## Carls Mini X 2

![](earthmoji.png)

Formatet driller lidt, så se den i fuld skærm:)

[Klik her for at se mit program køre](https://carlvestbo.gitlab.io/aestetiskprogrammering/MiniX2/index.html)

[Klik her for at se min kode](https://gitlab.com/carlvestbo/aestetiskprogrammering/-/blob/main/MiniX2/minix2.js)

### Beskrivelse af programmet 
Mit program har fået navnet E.arth.MOJI og består af en jordklode-emoji hvis ansigtsudtryk ændrer sig når man rykker i en slider der viser det antal CO2-ton man udleder om året. Jo mere man udleder desto mere vred bliver emojien. Derudover er der også lidt `text`der beskriver programmets formål samt en counter der viser `slider value`. Til venstre for emojien er der også et `link`man kan trykke på, som leder en hen til en hjemmeside hvor man kan regne sit eget forbrug ud. 

Til koden har jeg primært haft fokus på at benytte `if`-statements på de forskellige måder, som vi har lært om i undervisningen. De 4 `if` og `if else`-statements bliver kontrolleret af value (`val`) på min `slider`. Emojiens hoved består af 3 forskellige billeder af jordkloden som jeg har lavet udenfor VSC, da det var for svært at programmere. Øjnene består 4 `ellipse`, 2 store hvide og 2 små sorte, hvis position ikke ændrer sig i koden. 

### En interessant oplevelse
Da jeg opsatte det sidste `if`-statement med teksten "END OF HUMANITY", blev det venstre øje lige pludseligt rødt. Jeg kunne ikke forstå hvorfor den gjorde det og koden kunne ikke at køre uden øjet var rødt. Jeg sad ret lang tid med og blev faktisk mere og mere fan af udtrykket det gav, så besluttede til sidst bare at lade det være.


### Formål og refleksioner
Mit programs formål er i høj grad at skabe bevidsthed hos det enkelte individ omkring ens personlige klima-aftryk. Den provokerer samtidig også, da de forskellige emojis indikerer at det er bedst at lægge i den lave ende ift. udledning af CO2, hvilket en stor del af dem der ville benytte sig af mit program, højst sandsynligt ikke vil være. Samtidig giver den også dårlig samvittighed hos brugeren ved at fremvise jorden som glad og i de farver vi kender, til at være vred og stå i flammer, hvis man er i den høje ende af skalaen. Derudover viser den teksten "END OF HUMANITY" når man rammer maks på 17 CO2-ton årligt (hvilket er den gennemsnitlige danskers forbrug). 
E.arth.MOJI taler ind i et af samtidens vigtigste emner, nemlig klimaforandringer. Et meget komplekst emne, som bliver håndgribelig gjort ved at nedkoge klimakrisen til emojis på bedste "generation z"-vis. Dette kan i sig selv være en grotesk måde at belyse emnet på, men samtidig kan det også være en måde hvorpå man kan oplyse nogle flere omkring konsekvenserne af deres forbrug, som måske ellers har følt at emnet var for kompliseret at sætte sig ind i. 

Emojis kan have meget magt i forbindelse med at levere et budskab. Det beskrives i grundbogen at emojis både kan være sjove men også har egenskaben til at leverer vigtige budskaber: "These have become pervasive in communication, and are no longer simply typographic, but actual pictures which can be funny at times as emojis, but also come with underlying issues related to the politics of representation."(Soon & Cox,2020). I ens messenger kan det være redskabet der sætter tonen i beskeden, samtidig med at det kan være et fjollet element uden betydning. I mit program bliver jordkloden menneskeliggjort ved at give den et par øjne og en mund, og derved bliver menneskehedens største krise forvandlet til en emoji der forhåbentligt kan gøre emnet mere spiseligt for den almindelige dansker.



### Referencer

* Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020
* https://p5js.org/reference/
* https://www.un.org/en/climatechange/what-is-climate-change
* https://concito.dk/bliv-klimaklog/hvad-kan-man-selv-goere


