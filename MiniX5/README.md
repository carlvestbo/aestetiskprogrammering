## Mini X 5: A generative program

![](minix5.png)

[Klik her for at køre mit program](https://carlvestbo.gitlab.io/aestetiskprogrammering/MiniX5/index.html)

(reload siden for at generere et nyt værk)

[Klik her for at se min kode](https://gitlab.com/carlvestbo/aestetiskprogrammering/-/blob/main/MiniX5/minix5.js)

#### Beskrivelse af programmet

Til mit generative program har jeg skrevet en kode der ved brug af `random`-funktionen skaber et unikt kunstværk bestående af et `nested for-loop` med en masse `rect` som bliver tildelt en tilfældig(random) værdi for `width`og `height`, som derved skaber en ny konstellation af en masse firkanter hver gang man reloader siden. 

``````
//tegner firkanterne med random, hvor de kan have en width op til 5 og en height op til 10
//floor-funktionen bruges til at konvertere de random-tal til hele tal
push();
translate(i + størrelse/2, j + størrelse/2);
rect(0,0, størrelse * floor(random(1,widthFirkant)), størrelse * floor(random(1,widthFirkant)))
pop();
``````

`random`-funktionen bliver også brugt til at bestemme RGB-værdierne i koden. Jeg oplevede at farverne på firkanterne af en eller anden grund blev meget hvide hvis jeg bare skrev `random(0,255)`. Derfor gav jeg dem, efter at have fundet lidt hjælp på stack overflow, en startværdi der kunne hjælpe med at løse dette problem.

#### Refleksioner om generativitet

At tilføje et generativt element i sin kode er på mange måder som at slippe kontrollen. Når man ellers skrive kode, så er Visual Studio Code din trofaste hjælper der adlyder din ordre til punkt og prikke, den gør præcis hvad du beder den om. Men når man så tilføjer elementer som `random` eller `noise`, så bliver outputtet pludselig til noget som man ikke kan forudsige. Selvom at `random` i virkeligheden egentligt bare er pesudo-random, da computeren generere talene ud fra nogle parameter, så opfattes det for os mennesker stadig som ren tilfældighed: 
"In computational practice, randomness is not a matter of chance, but rather a matter of algorithm and implementation." (Montfort,s. 119, 2012).
Og dette kan hjælpe den digitale kunstner til at skabe nye og spændende resultater udfra ens grundidé:
"The auto-generator functions as a kind of co-creator, producing outputs that are shaped by the artist's initial parameters but also incorporating chance and randomness." (Soon & Cox, s. 128, 2020).

Denne tankegang om at auto-generatoren bliver til en medskaber af værket, synes jeg er meget sigende for ugens opgave. I mit program fortæller jeg computeren at den skal lave de her firkanter som ved brug af `random` skaber et nyt værk hver gang jeg indlæser siden. Når jeg sidder og bliver ved med at trykke `cmd+R` glemmer jeg helt at det er mig selv der har skrevet koden. For er det er mig eller computeren der har skabt værket? Dette spørgsmål diskuteres også i kapitlet: "The auto-generator challenges our assumptions about the nature of creativity and the boundaries between human and machine intelligence, inviting us to rethink our relationship with technology and the role it plays in shaping culture." (Soon & Cox, s. 141, 2020). Jeg ville højst sandsynligt ikke komme frem til et lignende resultat hvis jeg hardcodede det hele selv. Men dette er også et af de mere komplekse spørgsmål inden for digital kunst, da det er relativt nemt at skabe det samme værk på en anden computer. Der er ikke penselstrøg der kan være med til at give kunsteren "stamp of approval " ift. at det rent faktisk er deres egen kode. 



Selvom at generative elementer i kodning kan være med til udfordre kreativiteten i selve det at skabe kunst, da computeren overtager opgaven som ellers har været menneskets, så åbner det samtidig også op for nye muligheder for at skabe innovative og særprægede kunstværker som både kan styrke menneskers kreativitet, men også den digitale udvikling. 


#### Referenceliste

* Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142

* Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, (Cambridge, MA: MIT Press, 2012), 119-146.
