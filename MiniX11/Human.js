class Human {
    constructor() {
        //startposition
        this.posX = random(0,windowWidth);
        this.posY = random(0,windowHeight);
        this.retningx = random(-1,1);
        this.retningy = random(-1,1);
        this.speed = 3.8;
        this.size = 20;
        
        this.infected == false;
        this.rich == false;
        this.poor == false;


     
    }

    move(){    
    this.posX = this.posX + (this.retningx*this.speed);
    this.posY = this.posY + (this.retningy*this.speed);


    // 4 if-statements for hver side i canvas der alle ganger med -1 for at skifte retning
    if (this.posX - this.size/2 < 0){
        this.retningx = this.retningx * -1;
        }
        if (this.posX + this.size/2 > width){
        this.retningx = this.retningx * -1;
        }
        if (this.posY - this.size/2 < 0){
        this.retningy = this.retningy * -1;
        }
        if (this.posY + this.size/2 > height){
        this.retningy = this.retningy * -1;
        }

    }
 


    show() {
      

        if (this.infected == true) {

            // Display som virus
            fill(255, 0, 0); // Rød farve
            ellipse(this.posX, this.posY, this.size);
            fill(0);
            ellipse(this.posX, this.posY, this.size/1.5);

            if (this.rich == true) {

            fill(255);
            ellipse(this.posX, this.posY, this.size/1.5);
            } else if (this.poor == true) {
              fill(0); 
                ellipse(this.posX, this.posY, this.size /1.5);
            }
  
          } else if (this.rich == true) {
            // Display som rich
          
            fill(255); 
            ellipse(this.posX, this.posY, this.size);
           } else if (this.poor == true) {
                // Display som poor
                fill(0); 
                ellipse(this.posX, this.posY, this.size);
            }
        

  
          }
        }
        

       
    
