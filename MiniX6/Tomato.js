class Tomato {
    constructor() {
        //startposition
        this.posX = random(300,600);
        this.posY = random(200,400);
        this.retningx = 1;
        this.retningy = -1;
        this.speed = 6;
    }

    move(){    
    this.posX = this.posX + (this.retningx*this.speed);
    this.posY = this.posY + (this.retningy*this.speed);


    // 4 if-statements for hver side i canvas der alle ganger med -1 for at skifte retning
    if (this.posX < 0){
        this.retningx = this.retningx * -1;
        blip.play();
        }
        if (this.posX > width){
        this.retningx = this.retningx * -1;
        blip.play();
        }
        if (this.posY < 0){
        this.retningy = this.retningy * -1;
        blip.play();
        }
        if (this.posY > height){
        this.retningy = this.retningy * -1;
        gameover.play();
        textAlign(CENTER);
        textSize(60);
        text("GAME OVER", width/2, height/2);
        fill(255);
        noLoop()
        }
        
        }
    

    show() {
        fill(0, 255, 255);
        noStroke();
        ellipse(this.posX, this.posY, 50, 50);
        fill(0);
        ellipse(this.posX, this.posY, 35, 35);
        fill(255);
        ellipse(this.posX, this.posY, 20, 20);
    }
}
