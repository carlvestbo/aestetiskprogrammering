let tomato = [];
let player;
let point = 0;
let antal = 1;

//Preloader alle mine lyde og tilpasser volume
function preload(){
    move = loadSound("move.mp3");
    pointLyd = loadSound("point.mp3");
    blip = loadSound("blip.mp3");
    gameover = loadSound('gameover.wav');
    pointLyd.setVolume(0.5);
    blip.setVolume(0.5);
    move.setVolume(0.2);
    gameover.setVolume(0.5);
}


function setup() {
    createCanvas(1200, 800);
    for (let i = 0; i < antal; i++) {
        tomato.push(new Tomato());
        

    }

    player = new Player();

}

function draw() {
    background(0,60);
    spawnTomato();
    player.show();
    checkCollision();
    textAlign();
    textSize(50);
    fill(255);
    text("Point: "+point, width/2-50, 100)


}

function spawnTomato() {
    for (let i = 0; i < tomato.length; i++) {
        tomato[i].move();
       tomato[i].show();
        
    }
}


function keyPressed() {
    if (keyCode === LEFT_ARROW) {
        player.moveLeft();
        
    }
    if (keyCode === RIGHT_ARROW) {
        player.moveRight();
        
    }
    if (keyCode === ENTER){
        setup();
        spawnTomato();
       player.show();
       checkCollision();
    }
}

//laver funktionen collision der bestemmer at når spilleren og bolden kolliderer skal bolden bevæge sig i en anden retning
function checkCollision() {
    let distance;
    for (let i = 0; i < tomato.length; i++) {
        distance = dist(player.posX + 120/2, player.posY + 5/2, tomato[i].posX, tomato[i].posY);

        if (distance < 50-player.size/2) {
            tomato[i].retningy = tomato[i].retningy * -1;
            point++
            //pointLyd skal kun afspille hvis ikke den gør det i forvejen for at undgå den spiller oven i hinanden
            if(pointLyd.isPlaying() == false){
            pointLyd.play();
            }
                  
        }
    }
}
