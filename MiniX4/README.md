## Mini X 4

[Klik her for at køre mit program](https://carlvestbo.gitlab.io/aestetiskprogrammering/MiniX4/index.html)

[Klik her for at se min kode](https://gitlab.com/carlvestbo/aestetiskprogrammering/-/blob/main/MiniX4/minix4.js)

#### Beskrivelse af programmet
Til udstillingen CAPTURE ALL har jeg lavet et værk som jeg har givet navnet "WE ARE WATCHING YOU". Formålet med værket er at vise alt den data som apps, websites og stater får adgang til når man som forbruger giver adgang til, når man trykker ja til de uskyldige "cookies". Hvad giver denne knap egentligt adgang til, og hvordan bruges din data imod dig, både på godt og ondt? Man får at vide at det er for at forbedre ens online-oplevelser med bl.a. målrettet reklamer, men din data kan samtidig også bruges til at manipulere dig til at optage dyre kviklån eller udnytte dine ludomanske tendenser som gamblingselskaber betaler dyrt for at ramme præcis dig. Samtidig er der manglende lovgivning på data-området i mange dele af verden, hvor man som i Kina bliver målt og vejet i den digitale verden og med ansigtsgendkendelse bliver monetoreret og behandlet bedre hvis man retter ind efter deres ideologier. Derudover har de beføjelserne til at give dig en digital mundkurv på, ved brug af avancerede algoritmer som filtrerer kritisk content fra deres platforme. Værket "WE ARE WATCHING YOU", giver det frie valg tilbage til forbrugeren. Vil man have sandheden at vide om hvordan ens data bliver brugt, eller vil man forblive i den illusion som magthaverne har skabt.


#### Hvad indeholder koden
Til min kode har jeg benyttet mig af en række nye funktioner. Ved brug af det nye bibliotek `clmtrackr`giver det mulighed for at lave facetracking i realtid via videokamera og samtidig bruges der geometriske former og tekst, hvis position ændrer sig efter ens bevægelser. Derudover bruges `createButton`også til at skabe de to knapper. Her bruges der også `if`-statements til at aktivere og deaktivere de forskellige tekster, geometriske former og filter ved som alle er bundet op på variablen `truth`:

`````
//knap1-elementer
knap1 = createButton("YES"); 
knap1.position(100, 400); 
knap1.style("background-color","#ff0d19");
knap1.size(100, 50); 
knap1.mousePressed(() => truth = true); 
`````
Med `clmtrackr`er der nogle indbyggede funktioner der gør det muligt at tracke ens ansigt med `ctracker.getCurrentPosition`. Denne bliver brugt ved at sige den er `=` en variabel jeg har kaldt `positioner`. Dette bruges så i et `for-loop`til at skabe den rette mængde punkter omkring ansigtet:

`````
 positioner = ctracker.getCurrentPosition(); 
if(positioner.length){
for (let i = 0; i < positioner.length; i++) {
ellipse(positioner[i][0], positioner[i][1], 5); 
`````

#### Udfordringer
Det har været sjovt, men også udfordrende at bruge de nye biblioteker. Indtil videre i undervisningen har jeg haft rimelig godt styr på de funktioner vi har lært om, hvor coding train-videoerne også har hjulpet godt med det. Så i det at clmtrackr er ret omfattende og væsentligt mere avanceret end resten har jeg ikke altid helt forstået hvad jeg egentligt lavede med koden, hvilket var lidt frustrerende når koden ikke virkede. Derudover skulle jeg også have tungen lige i munden da jeg oploade koden til Gitlab ift. at tingene var i de rigtige mapper.


#### Refleksioner
Jeg synes ugens opgave har været spændende da det er et utroligt relevant emne, som også har været meget omdiskuteret med Cambridge Analytica, GDPR-lovgivningen og TikTok, for at nævne et par stykker. Bevidstheden omkring hvad vores data bliver brugt til, er noget de færreste tænker over i deres hverdag og mange selskaber pakker det utroligt godt ind og går det besværligt at gennemskue hvad deres "cookies" giver adgang til. Dette har jeg forsøgt at belyse med mit projekt, ved at skabe et program med en utrolig høj og ærlig gennemsigtighed. Med en reference til filmen The Matrix's ikoniske dilemma, "Red pill or blue pill?", bliver det muligt for forbrugerne at finde ud af hvad de egentligt  siger ja til og samtidig gør dem opmærksomme på intet i livet er gratis, og at deres data bliver brugt af tech-mastodonter til at skabe profit.



![](minix41.png)
![](minix42.png)

