//variabler

let ctracker; 

let knap1; 
let knap2;

let positioner; 
let truth; 

function setup() {

createCanvas(640, 480); 

//knap1-elementer
knap1 = createButton("YES"); 
knap1.position(100, 400); 
knap1.style("background-color","#ff0d19");
knap1.size(100, 50); 
knap1.mousePressed(() => truth = true); 

//knap2-elementer
knap2 = createButton("NO"); 
knap2.position(400, 400); 
knap2.style("background-color","#1e0afa");
knap2.size(100, 50); 
knap2.mousePressed(() => truth = false); 

//video
  video = createCapture(VIDEO); 
  video.size(640, 480); 
  video.hide(); 

 //tracker
ctracker = new clm.tracker(); 
ctracker.init(pModel); 
ctracker.start(video.elt); 
}

function draw() {

  image(video, 0, 0, 640, 480); 

// Tekst
  push(); 
  textSize(50); 
  textFont("Times New Roman");
  fill(0); 
  text('Want the truth?', 170, 100)
  pop(); 

 positioner = ctracker.getCurrentPosition(); 
if(positioner.length){
for (let i = 0; i < positioner.length; i++) {
ellipse(positioner[i][0], positioner[i][1], 5); 

} 
    
}
//to if-statement der viser hver sin tekst
    if (truth === true) {

  //filter
filter(INVERT);
textSize(30); 
  textFont("Times New Roman");
  fill(255,0,0); 
  text('How we see you', 200, 400)

    }

    if (truth === false) {
      textSize(30); 
  textFont("Times New Roman");
  fill(0,0,255); 
  text('How you see yourself', 170, 380)

    }

    rectMode(CENTER);
    if (truth === true) {

      fill(0);
       rect(positioner[60][0], positioner[60][1],75,20,0,0);

       fill(100);
       rect(positioner[60][0], positioner[55][1],75,20,0,0);

       textAlign(CENTER);
       fill(255);
       text('shush',positioner[60][0], positioner[55][1]);
       
    }
  }