let = startside = 0 // Startsiden til vores program
let human = []; // Array til vores Human class
let richAntal = 10; // Antal af rige humans
let poorAntal = 40; // Antal af fattige humans
let infektionsTid = 0; // Infektionstiden

function preload(){
  drone = loadSound("drone.wav");
}

function setup() {
  // put setup code here
  createCanvas(windowWidth,windowHeight);


}

function mousePressed(){
    if (startside === 0) {
    startProgram();
    drone.play();
  }
}


function startProgram() {

 
  startside = 1;

  // Pusher en infected human(patient 0) til start

  let infectedHuman = (new Human());
  infectedHuman.infected = true;
  human.push(infectedHuman);
  
  // Pusher 25 rige humans
  for (let i = 0; i < richAntal; i++) {
  let richHuman = (new Human());
  richHuman.rich = true;
  human.push(richHuman);
  }

  // Pusher 25 fattige humans
  for (let i = 0; i < poorAntal; i++) {
    let poorHuman = (new Human());
    poorHuman.poor = true;
    human.push(poorHuman);
    }

}


function spawnHuman() {
  //Spawn humans på canvas
  for (let i = 0; i < human.length; i++) {
      human[i].move();
     human[i].show();
      
  }
}

function draw() {

  // Starsiden
  if (startside === 0) {
    push();
    background(255);
    fill(0);
    textSize(20);
    textAlign(CENTER);
    textFont('Courier');
    text('Press anywhere to release patient 0',width/2,height/1.4);
    textSize(50);
    text('Virus',width/2,height/6);
    textSize(20);
    text('A generative simulation of a viruses impact on different social classes,',width/2,height/4);
    text('showcasing the inequality in society through their chance of survival',width/2,height/3.25);
    text('_______________________________________________________________________________________________',width/2,height/2.75);
    text('_______________________________________________________________________________________',width/2,height/1.6);
    textSize(25);
    text('Top 20%',width/2-400,height/2.25);
    text('Bottom 80%',width/2,height/2.25);
    text('Patient 0',width/2+400,height/2.25);
    textSize(20);
    text('Chance of survival: 80%',width/2-400,height/1.75);
    text('Chance of survival: 20%',width/2,height/1.75);
    text('Red outer circle = infected',width/2+400,height/1.75);

    fill(255);
    ellipse(width/2-400, height/2, 20);
    fill(0);
    ellipse(width/2, height/2, 20);
    fill(255,0,0);
    ellipse(width/2+400, height/2, 20);
    fill(0);
    ellipse(width/2+400, height/2, 20/1.5);

    pop();

  
    // Programmet starter hvis der bliver trykket på skærmen  
  } else if (startside === 1) {

  background(255,60);
  spawnHuman();

// Tæller og viser antal overlevende humans
  let survivorsRich = 0;
  let survivorsPoor = 0;
    for (let i = 0; i < human.length; i++) {
      if (human[i].rich) {
        survivorsRich++;
      } else if (human[i].poor) {
        survivorsPoor++;
      }
    }
  // Counteren i venstre hjørne
  textSize(20);
  fill(0);
  textFont('Courier');
  textAlign(LEFT, TOP);
  text("Top 20% survivors: " + survivorsRich + "/10" , 10, 10);
  text("Bottom 80% survivors: " + survivorsPoor + "/40", 10, 40);

  // Tæller antal inficerede humans udfra human.length 
  // Når antallet er 0 vises slutskærm med procent overlevende
  let inficeredeAntal = 0;
  for (let i = 0; i < human.length; i++) {
    if (human[i].infected) {
      inficeredeAntal++;
    }
  }
  if (inficeredeAntal === 0){
    textFont('Courier');
    fill(0);
    textAlign(CENTER);
    textSize(50);
    text("The virus was defeated", width/2, height/2 - 100);

    textSize(25);
    // Udregner antal overlevende i procent
    text(survivorsRich / richAntal * 100 + " % " + "of top 20% survived" , width/2, height/2);
    text(survivorsPoor / poorAntal * 100 + " % " + "of bottom 80% survived" , width/2, height/2 + 60);
  }

  console.log(infektionsTid)
 
// Hvis en human bliver infected, start timeren for den enkelte human(infektionsTid++)
  for (let i = 0; i < human.length; i++) {
    if (human[i].infected) {
      human[i].infektionsTid++;
      
      //Efter 5 sekunder er human ikke længere infected
      if (human[i].infektionsTid >= 300) { // 5 sekunder (framerate på 60)
        human[i].infected = false;

        //Procent chance for at rige og fattige dør af virus
        let randomNumber;
        randomNumber = floor(random(1,100));
        let oddsRich = 80;
        let oddsPoor = 20;

        console.log(randomNumber)


        if (human[i].rich) {
          if (randomNumber > oddsRich) {
            //fjerner den konkrete human fra arrayet, antallet er altid 1
            human.splice(i, 1);

          }  else {
            //Hvis ikke den bliver spliced = gå tilbage til at være rich
          human[i].poor = false;
          human[i].rich = true;

          }
        // Samme princip som ovenstående, bare for poor
        } if (human[i].poor) {
          if (randomNumber > oddsPoor) {
            human.splice(i, 1);

        } else { 
          human[i].rich = false;
          human[i].poor = true;
        }
      }
    
      }
    }
  //Tjekker collision i draw
  checkCollision();
 
    }
  }


function checkCollision() {
  let distance;
  // Nested forloop gør at en human ikke tjekker med sig selv men med andre 
  // Og at der sker den rigtige kollision
  for (let i = 0; i < human.length; i++) {
    for (let j = 0; j < human.length; j++) {
      if (i !== j) {
      //Beregner distancen mellem two humans
      distance = dist(human[i].posX, human[i].posY, human[j].posX, human[j].posY);
      
      // Hvis distancen mellem to humans er mindre end begges radius
      if (distance < human[i].size/2 + human[j].size/2) {

          //Skift tilfældig retning
          human[i].retningy = random(-1,1);
          human[i].retningx = random(-1,1);

          
      }
      // Hvis distancen mellem to humans er mindre end begges radius
      // Og hvis en er infected
      if (distance < human[i].size/2 + human[j].size/2) {
        if (human[i].infected == true) {

            // Start timeren
            human[i].infektionsTid = 1; 
            // skift human til infected
            human[i].infected = true; 
            human[j].infected = true; 

            //Skift tilfældig retning
            human[i].retningy = random(-1,1);
            human[i].retningx = random(-1,1);
      
        }
        }      
        }
         
      }
    }
  }
}

