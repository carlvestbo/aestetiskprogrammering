## Mini X 7

![](minix7.png)


[Klik her for at køre mit program](https://carlvestbo.gitlab.io/aestetiskprogrammering/MiniX7/index.html)

[Klik her for at se min kode](https://gitlab.com/carlvestbo/aestetiskprogrammering/-/blob/main/MiniX7/minix7.js)

[Klik her for at se min Player Class](https://gitlab.com/carlvestbo/aestetiskprogrammering/-/blob/main/MiniX7/Player.js)

[Klik her for at se min bold Class](https://gitlab.com/carlvestbo/aestetiskprogrammering/-/blob/main/MiniX7/Tomato.js)

##### Hvilken miniX har du valgt?
Jeg har valgt at genbesøge min mini x 6, hvor jeg lavede min egen version af spillet "pong". Det er i sin natur et meget simpelt spil, men jeg ville gerne forsøge at gøre spillet mindre statisk og samtidig skabe nogle features der kunne gøre spiloplevelsen bedre som helhed.

##### Hvilke ændringer har du lavet og hvorfor?
I mit spil har jeg lavet to forskellige tilføjelser. Den første tilføjelse jeg har lavet er en stigende hastighed på bolden. Dette har jeg lavet ved at sige `this.speed += 0.2;`. Selvom denne tilføjelse er virkelig simpel, så gør det en kæmpe forskel for spiloplevelsen og fikser netop problemet med at spillet er for statisk. Hver gang bolden ryger ind i en af væggene vil dens hastighed stige en smule, hvilket skaber en progression i spillet hvor det bliver sværere at nå at fange bolden. Derudover har jeg tilføjet en knap med funktionen `createButton`, som reloader hele siden når man trykker på den. Dette element har jeg tilføjet for at gøre det nemmere og mere intuitivt at påbegynde et nyt spil for brugeren.

``````
reloadButton = createButton('Prøv igen');
    reloadButton.position(600,810);
    reloadButton.mousePressed(reloadPage);
``````     
Så egentligt nogle relativt simple virkemidler til at få spillet til at fungere et hak bedre, men jeg synes det gør en stor forskel på selve spiloplevelsen.

##### Hvad har du lært i denne miniX? Hvordan kunne du inkorporere eller videreudvikle koncepterne i din ReadMe/RunMe baseret på litteraturen vi har haft indtil nu?
Mine primære overvejelser til ændringer i min mini x 6, har været i forhold til min RunMe, da jeg følte der var et par løse ender som ville gavne spillet hvis de blev lappet. Så med en inkorporering af flere syntakser er det lykkedes at skabe et program, der i højere grad end første udgave ændrer sig over tid, da version 2 af programmet har stor fokus på temporalitet. Endvidere kunne man i forhold til min ReadMe reflektere videre over om at nutidens spil er blevet forfinet, med for meget fokus på grafik og for lidt på spillets koncept, og argumentere for og imod denne strømning?


##### Hvad er relationen mellem æstetisk programmering og digital kultur? Hvordan demonstrere dit værk perspektiver i æstetisk programmeing (henvend evt. til forordet i grundbogen)?

I takt med at verden bliver mere og mere digitaliseret er det ufattelig vigtigt at have øje for hvornår og hvordan digitaliseringen påvirker os. Programmerne som næsten hele vores liv kører på i en eller anden grad, er og vil aldrig være neutrale. Fordomme og politiske og kulturelle holdninger skinner igennem det blå lys og æstetisk programmering kan være med til at belyse nogle af disse problemstillinger og derved skabe mere gennemsigtighed for forbrugerne. I teksten beskrives det ret godt hvilke aspekter af vores samfund og digitale kultur som æstetisk programmering vedrører:

"We follow the principle that the growing importance of software requires a new kind of cultural thinking, and curriculum, that can account for, and with which to understand better, from within, the politics and aesthetics of algorithmic procedures, data processing, and abstracted modeling." 

##### Referenceliste

* Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020

* https://p5js.org/reference/
