## Mini X 3

![](minix3.png)

[Klik her for at køre mit program](https://carlvestbo.gitlab.io/aestetiskprogrammering/MiniX3/index.html)

[Klik her for at se min kode](https://gitlab.com/carlvestbo/aestetiskprogrammering/-/blob/main/MiniX3/minix3.js)

I denne uge har vi fået til opgave at lave en throbber eller et loading icon som de fleste kender det som. Opgavebeskrivelsen:
"Use loops and any one of the transformational functions to redesign and program an “animated” throbber."


## Beskrivelse af programmet
Til min Mini X 3 har jeg lavet en throbber med en `ellipse` der flyver rundt i et uendelighedsmønster( et 8-tal der ligger ned). Nedenunder min throbber er der en tekst med ordene: "Just a few more minutes". Min `ellipse` har en fade-effekt hvilket bliver opnået ved brug af alpha i min `background`. 

I koden bruger jeg `sin` og `cos`  funktioner til at skabe uendelighedsmønsteret af en cirklen.
I draw()-funktionen bruges `sin` og `cos` til at beregne henholdsvis `x` og `y` positionen af cirklen. `frameCount`-variablen bruges som input til disse funktioner for at skabe den kontinuerlige cykliske bevægelse.

`````
 //variabel a gør at cirklen starter i midten af skærmen 
  let a = windowHeight / 2;
  //variabel for cirklens hastighed
  let hastighed = 0.025;
  //Bruger cos og sin til at skabe uendelighedsmønstret
  x = a * sin(frameCount * hastighed);
  y = a * sin(frameCount * hastighed) * cos(frameCount * hastighed);
``````

Det temporale element i min kode kommer ved brug af `frameCount` som i min kode er 60. Min `ellipse` bevæger sig ved at gange `frameCount` med min variabel som jeg har kaldt "hastighed", som overraskende nok styrer hastigheden som ellipsen flyver rundt med. 


## Refleksioner
Da jeg startede ud med at designe min throbber tænkte jeg at den gerne skulle være så simpel som muligt. Jeg eksperimenterede en del med `random`-funktionen både ift. farve og størrelsen på min `ellipse`, men fandt ud af at "less is more" når det kom til stykket. I min throbber vil jeg gerne belyse forskellen mellem mennesketid og computertid som reelt set er skabt af mennesker: "Technology has been a tool through which humans create dis-
tance from natural cycles and design their own time experiences."(Lammerant, s. 96, 2018). Dette har jeg forsøgt at opnå ved at programmere min `ellipse` til at bevæge sig i et uendelighedsmønster, som indikerer at den til evig tid vil "Loade". Samtidig har jeg skrevet teksten "Just a few more minutes" nedenunder, hvilket er computeren der forsikrer brugeren om at den snart er færdig med at loade, selvom min throbber aldrig bliver det. Dette clash i mellem de to elementer er endvidere med til at stille spørgsmålet om vi mennesker er for naive når det kommer til vores computere, idet vi stiller blindt på dem? Hvis throbberen tager for lang tid med at loade vil det udløse frustration hos brugeren, som jo har en anden forståelse af tid end computeren.

Throbberens vigtigste formål er for mig at forsikre mig om at computeren ikke er crashet, men blot er i gang med at "tænke". Men i virkeligheden viser throbberen ikke andet end at der i hvert fald sker noget inde bag skærmen og det kan derfor være svært at afkode hvilke mekanismer computeren foretager sig:

"It shows that a certain operation is in progress, but what exactly is happening, and how long this will take, isnot at all clear. There is no indication of progress or status as is the case with a progress bar,
for instance. We see the icon spinning, but it explains little about what goes on in the background or about timespan."(Soon et al., s. 74, 2020)

Forskellen i forståelse af tid bliver i min throbber synliggjort ved at parre uendelighedsthrobberen og teksten som står i stor kontrast til hinanden, netop for at vise at computerens temporale forståelse er konstrueret af mennesker. Som brugere af min throbber vil man naivt sidde og vente et par minutter mere på at den har loadet, selvom throbberen vil blive ved med at køre uendeligt.



## Referencer 

* https://p5js.org/reference/

* Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018)

* Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96
